# convert all algorithms to its related shape
# python convert.py coeff_file m,k,n out_file s1,s2,s3

SCRIPT="python convert.py"
DIR="algorithms"

$SCRIPT $DIR/bini322-10-52-approx 3,2,2 $DIR/bini232-10-52-approx 1,0,2
$SCRIPT $DIR/bini322-10-52-approx 3,2,2 $DIR/bini223-10-52-approx 1,2,0

$SCRIPT $DIR/smirnov224-13-91-approx 2,2,4 $DIR/smirnov242-13-91-approx 0,2,1
$SCRIPT $DIR/smirnov224-13-91-approx 2,2,4 $DIR/smirnov422-13-91-approx 2,0,1

$SCRIPT $DIR/smirnov225-16-124-approx 2,2,5 $DIR/smirnov252-16-124-approx 0,2,1
$SCRIPT $DIR/smirnov225-16-124-approx 2,2,5 $DIR/smirnov522-16-124-approx 2,0,1

$SCRIPT $DIR/smirnov272-22-198-approx 2,7,2 $DIR/smirnov227-22-198-approx 0,2,1
$SCRIPT $DIR/smirnov272-22-198-approx 2,7,2 $DIR/smirnov722-22-198-approx 1,0,2

$SCRIPT $DIR/smirnov323-14-108-approx 3,2,3 $DIR/smirnov332-14-108-approx 0,2,1
$SCRIPT $DIR/smirnov323-14-108-approx 3,2,3 $DIR/smirnov233-14-108-approx 1,0,2

$SCRIPT $DIR/smirnov334-27-202-approx 3,3,4 $DIR/smirnov343-27-202-approx 0,2,1
$SCRIPT $DIR/smirnov334-27-202-approx 3,3,4 $DIR/smirnov433-27-202-approx 2,0,1

$SCRIPT $DIR/smirnov442-24-180-approx 4,4,2 $DIR/smirnov424-24-180-approx 0,2,1
$SCRIPT $DIR/smirnov442-24-180-approx 4,4,2 $DIR/smirnov244-24-180-approx 2,0,1

$SCRIPT $DIR/smirnov552-37-262-approx 5,5,2 $DIR/smirnov525-37-262-approx 0,2,1
$SCRIPT $DIR/smirnov552-37-262-approx 5,5,2 $DIR/smirnov255-37-262-approx 2,0,1